﻿
using UnityEngine;
using UnityEngine.Events;

public class OutputVectorEvent:UnityEvent<Vector2,float>
{
}

public class LineInput : MonoBehaviour
{
    public MouseScreenPoint mouseScreenPoint;

    public Sprite arrowHead;

    private LineRenderer lineRenderer;

    public GameObject Head;

    private bool drawingLine;

    public OutputVectorEvent outputVectorEvent;

    // Initiliaze events
    private void Awake()
    {
        if (outputVectorEvent == null)
            outputVectorEvent = new OutputVectorEvent();
    }

    // Cache components
    // Add Listeners for mouse events
    void Start()
    {
        lineRenderer = GetComponent<LineRenderer>();
        
        mouseScreenPoint.OnMouseClicked.AddListener(DrawInitialLinePoint);
        mouseScreenPoint.OnMousePressed.AddListener(DrawLine);
        mouseScreenPoint.OnMouseReleased.AddListener(DrawLineEnd);
    }

    // When mouse registering input, draw the line
    private void Update()
    {
        if (mouseScreenPoint.drawing)
            DrawLine();
    }

    // Get the first position when mouse is clicked and set it to first point in line renderer
    private void DrawInitialLinePoint()
    {
        lineRenderer.SetPosition(0, mouseScreenPoint.GetScreenPoint());
    }

    // When mouse is being pressed and set it to the second point in line renderer
    private void DrawLine()
    {
        lineRenderer.SetPosition(1, mouseScreenPoint.GetScreenPoint());
    }

    // When mouse is released stop setting line renderer positions and generate output vector
    private void DrawLineEnd()
    {
        drawingLine = false;

        CreateOutputVector();
    }

    // Generates the vector output from the difference of second point and first point from mouse click
    // Pass the vector and magnitude of this difference
    public void CreateOutputVector()
    {
        Vector2 output = new Vector2((lineRenderer.GetPosition(1) - lineRenderer.GetPosition(0)).x,
                                    (lineRenderer.GetPosition(1) - lineRenderer.GetPosition(0)).y);

        outputVectorEvent?.Invoke(output, output.magnitude);
    }

    // Draw a arrow head on first position of the generated line vector
    private void DrawArrowHead()
    {
        Head.GetComponent<SpriteRenderer>().sortingOrder = 2;
        Head.transform.position = lineRenderer.GetPosition(1);
        Debug.Log(lineRenderer.GetPosition(1) - lineRenderer.GetPosition(0).normalized);
    }
}