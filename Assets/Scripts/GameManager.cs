﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public MouseScreenPoint mouseInput;

    public LineInput lineInput;

    public Button button;

    void Start()
    {
        button.OnStopMoving.AddListener(CanReadInput);
    }

    void CanReadInput()
    {
        mouseInput.canRegisterInput = true;
    }
}
