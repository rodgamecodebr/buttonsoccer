﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Button : MonoBehaviour
{
    public LineInput input;

    private Vector2 rightDir =      Vector2.right;
    private Vector2 upDir    =      Vector2.up;
    private Vector2 moveDirection;

    public float friction = 0.0001f;
    public float speed = 3.0f;

    private bool applyTranslation;

    public UnityEvent OnStopMoving;

    void Start()
    {
        if (input != null)
            input.outputVectorEvent.AddListener(Move);
    }

    void Update()
    {
        if (applyTranslation)
        {
            ApplyFriction();

            transform.Translate(moveDirection * Mathf.Abs(speed) * Time.deltaTime);
        }
    }

    public void Move(Vector2 direction, float sp)
    {
        this.speed = sp;
        this.moveDirection = direction;

        applyTranslation = true;
    }

    public void ApplyFriction()
    {
        if (speed > 0)
            speed -= friction + 0.005f;
        else
        {
            speed = 0f;
            applyTranslation = false;

            OnStopMoving?.Invoke();
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        Debug.Log(collision.collider.gameObject.name);
    }
}
